package com.wonders.xlab.cache.service;

import com.wonders.xlab.cache.entity.Member;
import com.wonders.xlab.cache.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * Created by wangqiang on 16/1/20.
 */
@Component
public class MemberService {

    @Autowired
    private MemberRepository memberRepository;

    @Cacheable(cacheNames = "memberCache", key = "'myapp:member:id:' + #p0")
    public Member retrieveMemberById(long id) {
       return memberRepository.findOne(id);
    }

}
