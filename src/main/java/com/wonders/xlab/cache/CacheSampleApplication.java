package com.wonders.xlab.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;

@SpringBootApplication
@EnableCaching
public class CacheSampleApplication {

    @Bean
    public CacheManager cacheManager(RedisConnectionFactory factory) {
        StringRedisTemplate template = new StringRedisTemplate(factory);
        template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        return new RedisCacheManager(template);
    }

	public static void main(String[] args) {
		SpringApplication.run(CacheSampleApplication.class, args);
	}
}
