package com.wonders.xlab.cache.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by wangqiang on 16/1/20.
 */
@Entity
@JsonIgnoreProperties("new")
public class Member extends AbstractPersistable<Long> {

    @Column(nullable = false)
    private String username;

    @Column(nullable = false, unique = true)
    private String mobiles;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "member_role",
        joinColumns = @JoinColumn(name = "member_id"),
        inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobiles() {
        return mobiles;
    }

    public void setMobiles(String mobiles) {
        this.mobiles = mobiles;
    }

    public Set<Role> getRoles() {
        if (roles != null) {
            return new HashSet<Role>(roles);
        }
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
