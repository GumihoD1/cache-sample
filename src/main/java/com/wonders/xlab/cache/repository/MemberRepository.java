package com.wonders.xlab.cache.repository;

import com.wonders.xlab.cache.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by wangqiang on 16/1/20.
 */
public interface MemberRepository extends JpaRepository<Member, Long> {
}
