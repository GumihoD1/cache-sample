package com.wonders.xlab.cache;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CacheSampleApplication.class)
@WebAppConfiguration
public abstract class CacheSampleApplicationTests {

    @Autowired
    private RedisConnectionFactory factory;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

	@Test
	public void contextLoads() {
        System.out.println("factory = " + factory);
        System.out.println("redisTemplate = " + redisTemplate);
        System.out.println("stringRedisTemplate = " + stringRedisTemplate);
    }

}
