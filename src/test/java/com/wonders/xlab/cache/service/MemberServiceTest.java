package com.wonders.xlab.cache.service;

import com.wonders.xlab.cache.CacheSampleApplicationTests;
import com.wonders.xlab.cache.entity.Member;
import com.wonders.xlab.cache.entity.Role;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

/**
 * Created by wangqiang on 16/1/20.
 */
public class MemberServiceTest extends CacheSampleApplicationTests {

    @Autowired
    private MemberService memberService;

    @Test
    public void testRetrieveMemberById() throws Exception {
        Member member = memberService.retrieveMemberById(1);
        System.out.println("member = " + member);
        System.out.println("member.getUsername() = " + member.getUsername());
        System.out.println("member.getMobiles() = " + member.getMobiles());

        Set<Role> roles = member.getRoles();
        for (Role role : roles) {
            System.out.println("role.getName() = " + role.getName());
        }
    }
}